<import resource="classpath:/alfresco/site-webscripts/org/alfresco/share/imports/share-header.lib.js">

/**
 * Override del metodo generateAppItems() per apportare le modifiche al menu
 */
function generateAppItems()
{
    var appItems = [
        {
            id: "HEADER_HOME",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_HOME",
                label: "header.menu.home.label",
                targetUrl: getUserHomeTargetUrl()
            }
        },
        {
            id: "HEADER_MY_FILES",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_MY_FILES",
                label: "header.menu.myfiles.label",
                targetUrl: "context/mine/myfiles"
            }
        },
        {
            id: "HEADER_SHARED_FILES",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_SHARED_FILES",
                label: "header.menu.shared.label",
                targetUrl: "context/shared/sharedfiles"
            }
        },
        {
            id: "HEADER_SITES_MENU",
            name: "alfresco/header/AlfSitesMenu",
            config: {
                id: "HEADER_SITES_MENU",
                label: "header.menu.sites.label",
                currentSite: page.url.templateArgs.site,
                currentUser: user.name,
                siteLandingPage: ""
            }
        },
        {
            id: "HEADER_TASKS",
            name: "alfresco/header/AlfMenuBarPopup",
            config: {
                id: "HEADER_TASKS",
                label: "header.menu.tasks.label",
                widgets: [
                    {
                        id: "HEADER_TASKS_GROUP",
                        name: "alfresco/menus/AlfMenuGroup",
                        config: {
                            widgets: [
                                {
                                    id: "HEADER_MY_TASKS",
                                    name: "alfresco/header/AlfMenuItem",
                                    config:
                                        {
                                            id: "HEADER_MY_TASKS",
                                            label: "header.menu.mytasks.label",
                                            iconClass: "alf-mytasks-icon",
                                            targetUrl: "my-tasks#filter=workflows|active"
                                        }
                                },
                                {
                                    id: "HEADER_MY_WORKFLOWS",
                                    name: "alfresco/header/AlfMenuItem",
                                    config:
                                        {
                                            id: "HEADER_MY_WORKFLOWS",
                                            label: "header.menu.myworkflows.label",
                                            iconClass: "alf-myworkflows-icon",
                                            targetUrl: "my-workflows#filter=workflows|active"
                                        }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ];
    if (user.isAdmin)
    {
        appItems.push({
            id: "HEADER_PEOPLE",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_PEOPLE",
                label: "header.menu.people.label",
                targetUrl: "people-finder"
            }
        });
    }
    if (user.isAdmin || showRepositoryLink == "true")
    {
        appItems.push({
            id: "HEADER_REPOSITORY",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_REPOSITORY",
                label: "header.menu.repository.label",
                targetUrl: "repository"
            }
        });
    }
    if (user.isAdmin)
    {
        appItems.push({
            id: "HEADER_ADMIN_CONSOLE",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_ADMIN_CONSOLE",
                label: "header.menu.admin.label",
                targetUrl: "console/admin-console/application"
            }
        });
    }
    /*
    * aggiunge allo header un elemento con il link a eCON
    */
    appItems.push({
        id: "HEADER_ECON_LINK",
        name: "alfresco/menus/AlfMenuBarItem",
        config: {
            id: "HEADER_ECON_LINK",
            label: "eCON",
            targetUrl: moduleProperties.getEConConsoleLink(),
            targetUrlType: "FULL_PATH" // {@link module:alfresco/enums/urlTypes#FULL_PATH}
        }
    });
    if (String(moduleProperties.plusFunctions) === 'true')
        /*
        * aggiunge allo header un elemento con il link all'aggiunta di documenti in eCON Plus
        */
        appItems.push({
            id: "HEADER_ECON_PLUS_ADD_DOCUMENTS_LINK",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_ECON_PLUS_ADD_DOCUMENTS_LINK",
                label: "Carica documenti",
                targetUrl: moduleProperties.getPlusAddDocumentsLink(),
                targetUrlType: "FULL_PATH" // {@link module:alfresco/enums/urlTypes#FULL_PATH}
            }
        });
    return appItems;
}

/**
 * Override del metodo getUserMenuWidgets() per apportare le modifiche al logout e al cambio password
 */
function getUserMenuWidgets()
{
    var userMenuWidgets = [
        {
            id: "HEADER_USER_MENU_DAHSBOARD",
            name: "alfresco/menus/AlfMenuItem",
            config: {
                id: "HEADER_USER_MENU_DASHBOARD",
                label: "header.menu.user_dashboard.label",
                iconClass: "alf-user-dashboard-icon",
                targetUrl: "user/" + encodeURIComponent(user.name) + "/dashboard"
            }
        },
        {
            id: "HEADER_USER_MENU_PROFILE",
            name: "alfresco/header/AlfMenuItem",
            config:
                {
                    id: "HEADER_USER_MENU_PROFILE",
                    label: "my_profile.label",
                    iconClass: "alf-user-profile-icon",
                    targetUrl: "user/" + encodeURIComponent(user.name) + "/profile"
                }
        },
        {
            id: "HEADER_USER_MENU_HELP",
            name: "alfresco/header/AlfMenuItem",
            config:
                {
                    id: "HEADER_USER_MENU_HELP",
                    label: "help.label",
                    iconClass: "alf-user-help-icon",
                    targetUrl: getHelpLink(),
                    targetUrlType: "FULL_PATH",
                    targetUrlLocation: "NEW"
                }
        },
        {
            id: "HEADER_USER_MENU_HOME_PAGE_GROUP",
            name: "alfresco/menus/AlfMenuGroup",
            config:
                {
                    label: "group.home_page.label",
                    widgets:
                        [
                            {
                                id: "HEADER_USER_MENU_SET_CURRENT_PAGE_AS_HOME",
                                name: "alfresco/header/AlfMenuItem",
                                config:
                                    {
                                        id: "HEADER_USER_MENU_SET_CURRENT_PAGE_AS_HOME",
                                        label: "set_current_page_as_home.label",
                                        iconClass: "alf-user-set-homepage-current-icon",
                                        publishTopic: "ALF_SET_CURRENT_PAGE_AS_HOME",
                                        publishPayload: {
                                            servletContext: page.url.servletContext
                                        }
                                    }
                            },
                            {
                                id: "HEADER_USER_MENU_SET_DASHBOARD_AS_HOME",
                                name: "alfresco/header/AlfMenuItem",
                                config:
                                    {
                                        id: "HEADER_USER_MENU_SET_DASHBOARD_AS_HOME",
                                        label: "set_dashboard_as_home.label",
                                        iconClass: "alf-user-set-homepage-dashboard-icon",
                                        publishTopic: "ALF_SET_USER_HOME_PAGE",
                                        publishPayload: {
                                            homePage: getUserHomePageDashboard()
                                        }
                                    }
                            }
                        ]
                }
        }
    ];
    if (!context.externalAuthentication)
    {
        var otherWidgets = [];
        if (user.capabilities.isMutable)
        {
            otherWidgets.push({
                id: "HEADER_USER_MENU_PASSWORD",
                name: "alfresco/header/AlfMenuItem",
                config:
                    {
                        id: "HEADER_USER_MENU_CHANGE_PASSWORD",
                        label: "change_password.label",
                        iconClass: "alf-user-password-icon",
                        targetUrl: "user/" + encodeURIComponent(user.name) + "/change-password"
                    }
            });
        }
        otherWidgets.push({
            id: "HEADER_USER_MENU_LOGOUT",
            name: "alfresco/header/AlfMenuItem",
            config:
                {
                    id: "HEADER_USER_MENU_LOGOUT",
                    label: "logout.label",
                    iconClass: "alf-user-logout-icon",
                    publishTopic: "ALF_DOLOGOUT"
                }
        });
        userMenuWidgets.push({
            id: "HEADER_USER_MENU_OTHER_GROUP",
            name: "alfresco/menus/AlfMenuGroup",
            config:
                {
                    label: "group.other.label",
                    widgets: otherWidgets,
                    additionalCssClasses: "alf-menu-group-no-label"
                }
        });
    } else
    {
        /*
         * modifiche alle voci di menu per cambio password e logout in presenza di un CAS esterno
         */
        userMenuWidgets.push({
            id: "HEADER_USER_MENU_PASSWORD",
            name: "alfresco/header/AlfMenuItem",
            config:
                {
                    id: "HEADER_USER_MENU_CHANGE_PASSWORD",
                    label: "change_password.label",
                    iconClass: "alf-user-password-icon",
                    targetUrl: casProperties.getPasswordLink(),
                    targetUrlType: "FULL_PATH" // {@link module:alfresco/enums/urlTypes#FULL_PATH}
                }
        });
        userMenuWidgets.push({
            id: "HEADER_USER_MENU_LOGOUT",
            name: "alfresco/header/AlfMenuItem",
            config:
                {
                    id: "HEADER_USER_MENU_LOGOUT",
                    label: "logout.label",
                    iconClass: "alf-user-logout-icon",
                    publishTopic: "ALF_DOLOGOUT"
                }
        });
    }
    return userMenuWidgets;
}

/**
 * Override del metodo getTitleBarModel() per rimuovere la possibilità di abbandorare i siti
 */
function getTitleBarModel()
{
    var titleConfig = [];
    if (page.titleId == "page.userDashboard.title")
    {
        // If the page is a user dashboard then make the customize dashboard item an
        // option...
        // NOTE: At the moment this is just a single menu item and not the child of a popup?
        // NOTE: Should this still be shown if the user is not the dashboard owner?
        var userDashboardConfiguration = {
            id: "HEADER_CUSTOMIZE_USER_DASHBOARD",
            name: "alfresco/menus/AlfMenuBarItem",
            config: {
                id: "HEADER_CUSTOMIZE_USER_DASHBOARD",
                label: "",
                title: msg.get("customize_dashboard.label"),
                iconAltText: msg.get("customize_dashboard.label"),
                iconClass: "alf-configure-icon",
                targetUrl: "customise-user-dashboard"
            }
        };
        titleConfig.push(userDashboardConfiguration);
    } else if (page.url.templateArgs.site != null)
    {
        // Create the basic site configuration menu...
        var siteConfig = {
            id: "HEADER_SITE_CONFIGURATION_DROPDOWN",
            name: "alfresco/menus/AlfMenuBarPopup",
            config: {
                id: "HEADER_SITE_CONFIGURATION_DROPDOWN",
                label: "",
                iconClass: "alf-configure-icon",
                iconAltText: msg.get("header.menu.siteConfig.altText"),
                title: msg.get("header.menu.siteConfig.altText"),
                widgets: []
            }
        };

        var siteData = getSiteData();
        if (siteData != null)
        {
            if (user.isAdmin && siteData.userIsMember && !siteData.userIsSiteManager)
            {
                // If the user is an admin, and a site member, but NOT the site manager then
                // add the menu item to let them become a site manager...
                siteConfig.config.widgets.push({
                    id: "HEADER_BECOME_SITE_MANAGER",
                    name: "alfresco/menus/AlfMenuItem",
                    config: {
                        id: "HEADER_BECOME_SITE_MANAGER",
                        label: "become_site_manager.label",
                        iconClass: "alf-cog-icon",
                        publishTopic: "ALF_BECOME_SITE_MANAGER",
                        publishPayload: {
                            site: page.url.templateArgs.site,
                            siteTitle: siteData.profile.title,
                            user: user.name,
                            userFullName: user.fullName,
                            reloadPage: true
                        }
                    }
                });
            }
            if (siteData.userIsSiteManager)
            {
                // If the user is a site manager then let them make customizations...
                // Add the invite option...
                titleConfig.push({
                    id: "HEADER_SITE_INVITE",
                    name: "alfresco/menus/AlfMenuBarItem",
                    config: {
                        id: "HEADER_SITE_INVITE",
                        label: "",
                        iconClass: "alf-user-icon",
                        iconAltText: msg.get("header.menu.invite.altText"),
                        title: msg.get("header.menu.invite.altText"),
                        targetUrl: "site/" + page.url.templateArgs.site + "/" + config.scoped["SitePages"]["additional-pages"].getChildValue("add-users")
                    }
                });

                // If on the dashboard then add the customize dashboard option...
                if (page.titleId == "page.siteDashboard.title" || page.titleId == "page.meeting_workspace.title")
                {
                    // Add Customize Dashboard
                    siteConfig.config.widgets.push({
                        id: "HEADER_CUSTOMIZE_SITE_DASHBOARD",
                        name: "alfresco/menus/AlfMenuItem",
                        config: {
                            id: "HEADER_CUSTOMIZE_SITE_DASHBOARD",
                            label: "customize_dashboard.label",
                            iconClass: "alf-cog-icon",
                            targetUrl: "site/" + page.url.templateArgs.site + "/customise-site-dashboard"
                        }
                    });
                }

                // Add the regular site manager options (edit site, customize site, leave site)
                siteConfig.config.widgets.push(
                    {
                        id: "HEADER_EDIT_SITE_DETAILS",
                        name: "alfresco/menus/AlfMenuItem",
                        config: {
                            id: "HEADER_EDIT_SITE_DETAILS",
                            label: "edit_site_details.label",
                            iconClass: "alf-edit-icon",
                            publishTopic: "ALF_EDIT_SITE",
                            publishPayload: {
                                site: page.url.templateArgs.site,
                                siteTitle: siteData.profile.title,
                                user: user.name,
                                userFullName: user.fullName
                            }
                        }
                    },
                    {
                        id: "HEADER_CUSTOMIZE_SITE",
                        name: "alfresco/menus/AlfMenuItem",
                        config: {
                            id: "HEADER_CUSTOMIZE_SITE",
                            label: "customize_site.label",
                            iconClass: "alf-cog-icon",
                            targetUrl: "site/" + page.url.templateArgs.site + "/customise-site"
                        }
                    }
                );

                // Add Leave Site options only if is direct member of the site
                if (siteData.userIsDirectMember)
                {
                    siteConfig.config.widgets.push({
                            id: "HEADER_DELETE_SITE",
                            name: "alfresco/menus/AlfMenuItem",
                            config: {
                                id: "HEADER_DELETE_SITE",
                                label: "delete_site.label",
                                iconClass: "alf-delete-20-icon",
                                publishTopic: "ALF_DELETE_SITE",
                                publishPayload: {
                                    shortName: page.url.templateArgs.site,
                                    redirect: {
                                        url: "user/" + encodeURIComponent(user.name) + "/dashboard",
                                        type: "PAGE_RELATIVE",
                                        target: "CURRENT"
                                    }
                                }
                            }
                        }
                        /* rimossa la possibilità di abbandonare il sito */
                        /*
                        {
                            id: "HEADER_LEAVE_SITE",
                            name: "alfresco/menus/AlfMenuItem",
                            config: {
                                id: "HEADER_LEAVE_SITE",
                                label: "leave_site.label",
                                iconClass: "alf-leave-icon",
                                publishTopic: "ALF_LEAVE_SITE",
                                publishPayload: {
                                    site: page.url.templateArgs.site,
                                    siteTitle: siteData.profile.title,
                                    user: user.name,
                                    userFullName: user.fullName
                                }
                            }
                        }*/
                    );
                }

            } else if (siteData.userIsMember && siteData.userIsDirectMember)
            {
                /* rimossa la possibilità di abbandonare il sito */
                /*
                // If the user is a member of a site then give them the option to leave...
                siteConfig.config.widgets.push({
                    id: "HEADER_LEAVE_SITE",
                    name: "alfresco/menus/AlfMenuItem",
                    config: {
                        id: "HEADER_LEAVE_SITE",
                        label: "leave_site.label",
                        iconClass: "alf-leave-icon",
                        publishTopic: "ALF_LEAVE_SITE",
                        publishPayload: {
                            site: page.url.templateArgs.site,
                            siteTitle: siteData.profile.title,
                            user: user.name,
                            userFullName: user.fullName
                        }
                    }
                });
                */
            } else if (siteData.profile.visibility != "PRIVATE" || user.isAdmin)
            {
                var pendingInvite = getPendingInvite(siteData.profile.visibility);
                if (pendingInvite.id !== undefined)
                {
                    siteConfig.config.widgets.push({
                        id: "HEADER_CANCEL_JOIN_SITE_REQUEST",
                        name: "alfresco/menus/AlfMenuItem",
                        config: {
                            id: "HEADER_CANCEL_JOIN_SITE_REQUEST",
                            label: "cancel_request_to_join.label",
                            iconClass: "alf-leave-icon",
                            publishTopic: "ALF_CANCEL_JOIN_SITE_REQUEST",
                            publishPayload: {
                                siteId: page.url.templateArgs.site,
                                siteTitle: siteData.profile.title,
                                pendingInvite: pendingInvite
                            }
                        }
                    });
                } else
                {
                    // If the user is not a member of a site then give them the option to join...
                    if (!siteData.userIsMember)
                    {
                        siteConfig.config.widgets.push({
                            id: "HEADER_JOIN_SITE",
                            name: "alfresco/menus/AlfMenuItem",
                            config: {
                                id: "HEADER_JOIN_SITE",
                                label: (siteData.profile.visibility == "MODERATED" ? "join_site_moderated.label" : "join_site.label"),
                                iconClass: "alf-leave-icon",
                                publishTopic: (siteData.profile.visibility == "MODERATED" ? "ALF_REQUEST_SITE_MEMBERSHIP" : "ALF_JOIN_SITE"),
                                publishPayload: {
                                    site: page.url.templateArgs.site,
                                    siteTitle: siteData.profile.title,
                                    user: user.name,
                                    userFullName: user.fullName
                                }
                            }
                        });
                    }
                }
            }
        }

        if (siteConfig.config.widgets.length > 0)
        {
            titleConfig.push(siteConfig);
        }

    }
    return titleConfig;
}

model.jsonModel = {
    rootNodeId: "share-header",
    services: getHeaderServices(),
    widgets: [
        {
            id: "SHARE_VERTICAL_LAYOUT",
            name: "alfresco/layout/VerticalWidgets",
            config:
                {
                    widgets: getHeaderModel()
                }
        }
    ]
};

if (context.externalAuthentication)
{
    /* sostituisce il servizio LogoutService di Alfresco con la versione custom Authentication/LogoutService */
    var modelServices = model.jsonModel.services;
    for (var i = 0, len = modelServices.length; i < len; i++)
    {
        if (typeof modelServices[i] === "string" && modelServices[i] === "alfresco/services/LogoutService")
        {
            model.jsonModel.services[i] = {
                name: "Authentication/LogoutService",
                config: {
                    "casLogout": casProperties.getCasServerLogoutUrl(),
                    "paramName": casProperties.getCasServiceParamName(),
                    "logoutDestination": casProperties.getLogoutDestination()
                }
            };
        }
    }
}