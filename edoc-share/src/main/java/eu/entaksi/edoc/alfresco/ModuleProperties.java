package eu.entaksi.edoc.alfresco;

import org.springframework.extensions.webscripts.processor.BaseProcessorExtension;

/**
 * Classe per accedere dai webscript alle proprietà specificate in module.properties.
 * Il mapping è presente in alfresco/web-extension/lul-share-slingshot-application-context.xml nel bean moduleProperties.
 */
public class ModuleProperties extends BaseProcessorExtension
{
    /**
     * Attiva le funzionalità di edoc-lul.
     */
    private Boolean lulFunctions;
    /**
     * Attiva le funzionalità di edoc-plus.
     */
    private Boolean plusFunctions;
    /**
     * Link alla console eCon.
     */
    private String eConConsoleLink;
    /**
     * Link alla funzionalità di aggiunta documenti di edoc-plus.
     */
    private String plusAddDocumentsLink;

    public Boolean getLulFunctions()
    {
        return lulFunctions;
    }

    public void setLulFunctions(Boolean lulFunctions)
    {
        this.lulFunctions = lulFunctions;
    }

    public Boolean getPlusFunctions()
    {
        return plusFunctions;
    }

    public void setPlusFunctions(Boolean plusFunctions)
    {
        this.plusFunctions = plusFunctions;
    }

    public String getEConConsoleLink()
    {
        return eConConsoleLink;
    }

    public void setEConConsoleLink(String eConConsoleLink)
    {
        this.eConConsoleLink = eConConsoleLink;
    }

    public String getPlusAddDocumentsLink()
    {
        return plusAddDocumentsLink;
    }

    public void setPlusAddDocumentsLink(String plusAddDocumentsLink)
    {
        this.plusAddDocumentsLink = plusAddDocumentsLink;
    }
}