# Le versioni successive alla 6.2.0 hanno questo problema: https://github.com/Alfresco/acs-community-packaging/issues/367
FROM alfresco/alfresco-share:6.2.0

ARG TOMCAT_DIR=/usr/local/tomcat

ARG SHARE_SITE_CREATORS_AMP=https://github.com/jpotts/share-site-creators/releases/download/0.0.7/share-site-creators-share-0.0.7.amp
ARG EDOC_THEME_VERSION="1.1.0"
ARG EDOC_THEME_AMP=https://maven.entaksi.eu/artifactory/entaksi-private-release/eu/entaksi/edoc/entaksi-edoc-theme/$EDOC_THEME_VERSION/entaksi-edoc-theme-$EDOC_THEME_VERSION.amp
ARG ARTIFACTORY_USER
ARG ARTIFACTORY_ENCRYPTED_PWD

RUN yum install -y wget ca-certificates && \
	yum clean all

# download dell'AMP Share Site Creators
RUN wget -P $TOMCAT_DIR/amps_share/ $SHARE_SITE_CREATORS_AMP

# download dell'AMP del tema eDoc
RUN wget --http-user=$ARTIFACTORY_USER --http-password=$ARTIFACTORY_ENCRYPTED_PWD \
  -O $TOMCAT_DIR/amps_share/entaksi-edoc-theme-$EDOC_THEME_VERSION.amp $EDOC_THEME_AMP

# copia degli AMP nella cartella dell'application server
COPY target/edoc-share-*.amp $TOMCAT_DIR/amps_share/

# installazione degli AMP su Share
RUN java -jar $TOMCAT_DIR/alfresco-mmt/alfresco-mmt*.jar install \
	$TOMCAT_DIR/amps_share $TOMCAT_DIR/webapps/share -\
  	-directory -force -nobackup

# patch per https://github.com/Alfresco/acs-community-packaging/issues/367
RUN sed -i 's@<show-authorization-status>true</show-authorization-status>@<show-authorization-status>false</show-authorization-status>@' \
	$TOMCAT_DIR/webapps/share/WEB-INF/classes/alfresco/share-config.xml

# rimozione di MessagesWebScript.class per consentirne la sovrascrittura con l'AMP
RUN rm $TOMCAT_DIR/webapps/share/WEB-INF/classes/org/alfresco/web/scripts/MessagesWebScript.class

# aggiunta del connector per il proxy keycloak
RUN sed -i "/<remote>$/a \
         <connector>\n\
             <id>alfrescoHeader</id>\n\
             <name>Alfresco Connector</name>\n\
             <description>Connects to an Alfresco instance using header and cookie-based authentication</description>\n\
             <class>org.alfresco.web.site.servlet.SlingshotAlfrescoConnector</class>\n\
             <userHeader>X-Forwarded-User</userHeader>\n\
         </connector>\n" \
         $TOMCAT_DIR/shared/classes/alfresco/web-extension/share-config-custom.xml && \
	# impostazione del connector per il proxy keycloak negli endpoint
	sed -i -E '\@<id>alfresco</id>$@,\@</connector-id>@s@(<connector-id>alfresco)@\1Header@' \
	$TOMCAT_DIR/shared/classes/alfresco/web-extension/share-config-custom.xml && \
	sed -i -E '\@<id>alfresco-api</id>$@,\@</connector-id>@s@(<connector-id>alfresco)@\1Header@' \
	$TOMCAT_DIR/shared/classes/alfresco/web-extension/share-config-custom.xml && \
	# aggiunta dell'autenticazione esterna per gli endpoint
	sed -i -E '\@alfrescoHeader</connector-id>$@,\@</identity>@s@(</identity>)@\1\n            <external-auth>true</external-auth>@' \
	$TOMCAT_DIR/shared/classes/alfresco/web-extension/share-config-custom.xml

# aumento della dimensione degli header accettati da tomcat (#8651, #8992)
RUN sed -i -E "/port=\"8080\" protocol=\"HTTP\/1\.1\"/a \
               maxHttpHeaderSize=\"65536\" " \
               $TOMCAT_DIR/conf/server.xml